--[[
Make metadata accessible in latex by defining macros:
\def\metakey{value}
--]]

local str = pandoc.utils.stringify

function cmd(k, v)
	return pandoc.MetaBlocks(pandoc.RawBlock("latex",
		"\\def\\meta"..k.."{"..v.."}\n"
	))
end

function Meta(m)
	-- which ones are multiline?
	local multiline = {}
	for k, v in pairs(m['multiline'] or {}) do
		multiline[str(v)] = true
	end

	-- read and define latex macros
	local cmds = {}
	for k, v in pairs(m) do
		local v_
		if multiline[k] then
			v_ = {}
			for j, w in pairs(v) do
				v_[#v_+1] = str(w)
			end
			v_ = table.concat(v_, "\\\\")
		else
            if type(v) == type(true) then
                v_ = v and "1" or "0"
            elseif type(v) == type("") then
                v_ = tostring(v)
            else
			    v_ = str(v)
            end
		end
		cmds[#cmds+1] = cmd(k, v_)
	end

	-- append to header-includes
	local hi = 'header-includes'
	m[hi] = m[hi] and {m[hi], cmds} or cmds
	return m
end
