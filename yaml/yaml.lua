--[[
    Within Math environments, replace 'meta.fieldname' surrounded by whitespace with the metadata field `fieldname`.

    Source: https://pandoc.org/lua-filters.html#replacing-placeholders-with-their-metadata-value
--]]

local vars = {}

local function Meta(meta)
    for k, v in pairs(meta) do
        vars[k] = v
    end
end

local function transform(label)
    local i = 0
    local selected = vars
    for word in string.gmatch(label, "[^.]+") do
        i = i + 1
        if i == 1 then
            if word ~= 'meta' then
                return label
            end
        else
            local number = tonumber(word)
            if number then
                selected = selected[number]
            elseif selected[word] then
                selected = selected[word]
            else
                return label
            end
        end
    end
    return pandoc.utils.stringify(selected)
end

local function Math(elem)
    local text = ""
    local last_end = 1
    for match in string.gmatch(elem.text, "meta%.[%w%._-]+") do
        local start, end_match = string.find(elem.text, match, last_end, true)
        text = text .. string.sub(elem.text, last_end, start-1)
        text = text .. transform(match)
        last_end = end_match + 1
    end
    text = text .. string.sub(elem.text, last_end)
    elem.text = text
    return elem
end

return {{Meta = Meta}, {Math = Math}, {RawBlock = Math}}
