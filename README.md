# Pandoc Filters by Desmond Kabus

This repository contains a random collection of [Pandoc](https://pandoc.org/)
filters. Their main use case are academic writing for mathematics/physics, but
there may be anything in here.
