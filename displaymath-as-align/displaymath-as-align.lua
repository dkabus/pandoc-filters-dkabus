--[[
    Convert math blocks surrounded by $$ $$ with \begin{align} \end{align}
--]]

function Math(elem)
    if elem.mathtype == 'DisplayMath' then
        local latex = '\\begin{align}' .. elem.text .. '\\end{align}'
        return pandoc.RawInline('latex', latex)
    end
end
