--[[
    Replace numbers followed by units separated by SEP by calls
    to the LaTeX siunitx package to display them in a fancy way.

    You can specify SEP in the metadata header of the pandoc document
    with the key 'units_filter_separator'. Usually, you want this
    because otherwise unwanted replacements may be done.

    Examples:

    - 10ms -> \qty{10}{\milli\second}
    - 1.2e-3mm² -> \qty{1.2e-3}{\milli\metre\squared}
--]]

local SEP = ''
local SCIENTIFIC_NUMBER = '[+-]?%d*%.?%d+[eE]?[%+%-]?%d*'

local BASE_UNITS = {
    m='\\meter',
    kg='\\kilogram',
    s='\\second',
    A='\\ampere',
    K='\\kelvin',
    mol='\\mole',
    cd='\\candela',
    Hz='\\hertz',
    N='\\newton',
    Pa='\\pascal',
    J='\\joule',
    W='\\watt',
    C='\\coulomb',
    V='\\volt',
    F='\\farad',
    S='\\siemens',
    T='\\tesla',
}
BASE_UNITS['Ω'] = '\\ohm'
BASE_UNITS['%%'] = '\\percent'
BASE_UNITS['°'] = '\\degree'
BASE_UNITS['°C'] = '\\celsius'

local DECIMAL_PREFIXES = {
    Q='\\quetta',
    R='\\ronna',
    Y='\\yotta',
    Z='\\zetta',
    E='\\exa',
    P='\\peta',
    T='\\tera',
    G='\\giga',
    M='\\mega',
    k='\\kilo',
    h='\\hecto',
    d='\\deci',
    c='\\centi',
    m='\\milli',
    u='\\micro',
    n='\\nano',
    p='\\pico',
    f='\\femto',
    a='\\atto',
    z='\\zepto',
    y='\\yocto',
    r='\\ronto',
    q='\\quecto',
}
DECIMAL_PREFIXES['μ'] = '\\micro'

local EXPONENTS = {}
EXPONENTS['²'] = '\\squared'
EXPONENTS['³'] = '\\cubed'

local UNITS = {}
for base_code, base_latex in pairs(BASE_UNITS) do
    table.insert(UNITS, {base_code, base_latex})
    for decimal_code, decimal_latex in pairs(DECIMAL_PREFIXES) do
        table.insert(UNITS, {decimal_code..base_code, decimal_latex..base_latex})
        for exp_code, exp_latex in pairs(EXPONENTS) do
            table.insert(UNITS, {decimal_code..base_code..exp_code, decimal_latex..base_latex..exp_latex})
        end
    end
end
table.sort(UNITS, function(a, b) return #a[1] > #b[1] end)

local function replace_units_para(elem)
    if elem.text:find(SEP) then
        for _, tuple in ipairs(UNITS) do
            local code = tuple[1]
            local latex = tuple[2]

            if elem.text:match(SCIENTIFIC_NUMBER..SEP..code) then
                local num = elem.text:match(SCIENTIFIC_NUMBER)
                return pandoc.RawInline('latex', '\\qty{'..num..'}{'..latex..'}'..elem.text:sub(#num+#SEP+#code+1))
            end
        end
    end
end

function Para(elem)
    local elems = {}
    for _, item in ipairs(elem.content) do
        table.insert(elems, item.t == 'Str' and replace_units_para(item) or item)
    end
    return pandoc.Para(elems)
end

local function replace_units_raw(text)
    if text:find(SEP) then
        for _, tuple in ipairs(UNITS) do
            local code = tuple[1]
            local latex = tuple[2]
            text = text:gsub('^('..SCIENTIFIC_NUMBER..')'..SEP..code, '\\qty{%1}{'..latex..'}')
            text = text:gsub('(%D)('..SCIENTIFIC_NUMBER..')'..SEP..code, '%1\\qty{%2}{'..latex..'}')
        end
    end
    return text
end

function Math(elem)
    if elem.text:find(SEP) then
        local text = ''
        local last_end = 1
        for match in elem.text:gmatch(SCIENTIFIC_NUMBER..SEP..'%w+') do
            local start, end_match = string.find(elem.text, match, last_end, true)
            text = text .. string.sub(elem.text, last_end, start-1)
            text = text .. replace_units_raw(match)
            last_end = end_match + 1
        end
        text = text .. string.sub(elem.text, last_end)
        elem.text = text
        return elem
    end
end

function Meta(meta)
    local sep = meta.units_filter_separator
    if sep then
        SEP = pandoc.utils.stringify(sep)
    end
end

return {
    {Meta=Meta},
    {Math=Math, Para=Para},
}
