--[[
    Replace `\vec` with `\symbfit`
--]]

function Math(elem)
    elem.text = elem.text:gsub("%f[%a]vec%f[%A]", "symbfit")
    return elem
end
