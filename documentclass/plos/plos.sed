#!/bin/sed -f
s/\\maketitle\>//;
0,/\\begin{document}/{s/\\usepackage\[utf8\]{inputenc}/\\usepackage\[utf8x\]{inputenc}/};
/\\begin{abstract}/,/\\end{abstract}/d;
/\\begin{flushleft}/,/\\end{flushleft}/{/^$/d};
s/\\date\s*{/\\lfoot{/;
