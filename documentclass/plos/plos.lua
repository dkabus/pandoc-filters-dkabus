META = {}

local footnote_symbol = {"\\Yinyang", "\\ddag", "\\dag", "\\textpilcrow"}

local latex = function (code) return pandoc.RawBlock('latex', code) end
local str = pandoc.utils.stringify

local function current_script()
  return debug.getinfo(2, "S").source:sub(2)
end

local function warn(...)
    io.stderr:write(table.concat(pandoc.List({'WARNING', current_script(), ...}):map(str), ' ')..'\n')
end
local function warn_missing(...) warn('missing key in metadata:', ...) end

local function is_valid_varname(varname)
    local valid = true
    if varname:sub(1,1):match('%a') then
        for i = 2, #varname do
            if not varname:sub(i,i):match('%w') then
                valid = false
                break
            end
        end
    else
        valid = false
    end
    return valid
end

local function metapath(...)
    local a = ''
    for i, key in ipairs({...}) do
        if type(key) == "number" then
            a = a..'['..key..']'
        elseif not is_valid_varname(str(key)) then
            a = a..'["'..key..'"]'
        else
            if i > 1 then
                a = a..'.'
            end
            a = a..key
        end
    end
    return a
end

local function metaget(...)
    local sel = META
    for _, key in ipairs({...}) do
        sel = sel[key]
    end
    assert(sel, 'MISSING IN METADATA!\t'..metapath(...))
    return sel
end

local function get_initials(name)
    local initials = ""
    for i = 1, #name do
        local char = name:sub(i, i)
        if char:match("%u") then
            initials = initials .. char
        end
    end
    return initials
end

local function fancy_concat(list, sep)
    local s = str(list[1])
    for i = 2, #list do
        if type(list[i-1]) == 'number' and type(list[i]) == 'number' then
            s = s..','
        end
        s = s..str(list[i])
    end
    return s
end

local function blocks_maketitle()
    local marks = {}
    local blocks = pandoc.List()
    local inlines = pandoc.Inlines{}
    for i, row in ipairs(metaget('institute')) do
        assert(type(row) == 'table', metapath('institute', i)..' must be a table!')
        for key, institute in pairs(row) do
            assert(type(institute) == 'table', metapath('institute', i, key)..' must be a table!')
            marks[key] = i
            inlines:extend{pandoc.Strong(str(marks[key])), pandoc.Space()}
            if institute.name then
                local address = pandoc.List()
                address:insert(institute.name)
                if institute.addressline then
                    address:insert(institute.addressline)
                else
                    warn_missing(metapath('institute', i, key, 'addressline'))
                end
                if institute.city then
                    if institute.postcode then
                        address:insert(str(institute.postcode)..' '..str(institute.city))
                    else
                        warn_missing(metapath('institute', i, key, 'postcode'))
                        address:insert(institute.city)
                    end
                else
                    warn_missing(metapath('institute', i, key, 'city'))
                end
                if institute.country then
                    address:insert(institute.country)
                else
                    warn_missing(metapath('institute', i, key, 'country'))
                end
                inlines:insert(pandoc.Str(table.concat(address:map(str), ', ')))
            else
                warn_missing(metapath('institute', i, key, 'name'))
            end
            inlines:insert(pandoc.LineBreak())
        end
    end
    inlines:insert(pandoc.RawInline('latex', '\\bigskip'))
    blocks:insert(pandoc.Para(inlines))

    local correspondence_counter = 0
    local correspondence_blocks = pandoc.List()
    local new_authors = pandoc.List()
    local footnote_count = 0
    local footnotes = {}
    inlines = pandoc.List()
    for i, author in ipairs(metaget('authors')) do
        assert(type(author) == 'table', metapath('author', i)..' must be a table!')
        assert(type(author.institute) == 'table' and #author.institute > 0, metapath('author', i, 'institute')..' must be a list!')

        if i > 1 then
            inlines:insert(pandoc.Str(', '))
        end

        local annotations = pandoc.List()
        for _, key in ipairs(author.institute) do
            local mark = marks[str(key)]
            assert(mark, 'Can not find an institute with key: '..str(key))
            annotations:insert(mark)
        end

        if author.correspondence then
            correspondence_counter = correspondence_counter + 1
            local mark = string.rep('*', correspondence_counter)
            annotations:insert(mark)
            correspondence_blocks:insert(pandoc.Para(pandoc.Inlines{
                mark, pandoc.Space(), pandoc.Str(str(metaget('author', i, author.name, 'email')))
            }))
        end

        if author.equal then
            if not author.footnotes then
                warn_missing(metapath('author', i, author.name, 'footnotes'))
                author.footnotes = {}
            end
            author.footnotes['_equal'..str(author.equal)] = 'These authors contributed equally to this work.'
        end

        if author.footnotes then
            for key, description in pairs(author.footnotes) do
                local k = str(key)
                if not footnotes[k] then
                    footnote_count = footnote_count + 1
                    footnotes[k] = {num=footnote_count, desc=str(description)}
                end
                annotations:insert(footnote_symbol[footnotes[k].num])
            end
        else
            warn_missing(metapath('author', i, author.name, 'footnotes'))
        end

        inlines:insert(pandoc.Str(str(author.name):gsub('{', ''):gsub('}', '')))
        inlines:insert(pandoc.Superscript(pandoc.RawInline('latex', fancy_concat(annotations, ','))))
        new_authors:insert(author.name)
    end
    inlines:insert(pandoc.RawInline('latex', '\n\\\\\n\\bigskip\n'))
    blocks:insert(1, pandoc.Para(inlines))

    if #META.authors < 1 then
        error('No authors given!')
    end

    local footnotes_sorted = {}
    for _, fn in pairs(footnotes) do
        footnotes_sorted[fn.num] = fn.desc
    end
    for num, desc in ipairs(footnotes_sorted) do
        blocks:insert(latex('\\par\n'..footnote_symbol[num]..' '..str(desc)..'\n'))
    end

    if correspondence_counter == 0 then
        warn_missing(metapath('author', 0, '???', 'correspondence'), '(There is no corresponding author!)')
    else
        blocks:insert(latex('\\par'))
        blocks:extend(correspondence_blocks)
    end

    blocks:insert(1, latex('{\\Large\n\\textbf\\newline{'..str(metaget('title'))..'}\n}\n\\newline\n\\\\'))

    blocks:insert(1, latex('\\begin{flushleft}'))
    blocks:insert(latex('\\end{flushleft}'))

    return {new_authors, pandoc.Blocks(blocks)}
end

local function blocks_author_contributions()
    local at_least_one = false
    local header = pandoc.Header(1, 'Author contributions')
    header.classes = {'unlisted', 'unnumbered'}

    local list = pandoc.List()
    for i, author in ipairs(META.authors) do
        if author.credit then
            at_least_one = true
            list:insert(pandoc.Strong(author.initials..': '))
            for j, contribution in ipairs(author.credit) do
                if j > 1 then
                    list:insert(pandoc.Str(', '))
                end
                list:insert(pandoc.Str(str(contribution)))
            end
            list:insert(pandoc.Str('. '))
        else
            warn_missing(metapath('author', i, author.name, 'credit'))
        end
    end

    if at_least_one then
        return pandoc.Blocks{header, list}
    else
        return {}
    end
end

local function blocks_text(heading, key)
    if not key then
        key = heading:lower():gsub(' ', '-')
    end
    if META[key] then
        local header = pandoc.Header(1, heading)
        header.classes = {'unlisted', 'unnumbered'}
        return pandoc.Blocks{header, table.unpack(META[key])}
    else
        warn_missing(metapath(key))
    end
    return {}
end

local function blocks_orcids()
    local at_least_one = false
    local header = pandoc.Header(1, 'ORCIDs')
    header.classes = {'unlisted', 'unnumbered'}

    local orcids = pandoc.List()
    for i, author in ipairs(META.authors) do
        if author.orcid then
            at_least_one = true
            local orcid = str(author.orcid)
            orcids:insert(pandoc.Para(pandoc.Inlines{
                pandoc.Link(orcid, 'https://orcid.org/'..orcid),
                pandoc.Str(':'), pandoc.Space(), pandoc.Str(str(author.initials))}))
        else
            warn_missing(metapath('author', i, author.name, 'orcid'))
        end
    end

    if at_least_one then
        return pandoc.Blocks{header, pandoc.BulletList(orcids)}
    else
        return {}
    end
end

function Meta(meta)
    META = meta

    for _, key in ipairs({'abstract', 'title', 'date', 'author'}) do
        if not META[key] then
            warn_missing(metapath(key))
        end
    end

    local blocks = pandoc.List()
    blocks:insert(pandoc.RawBlock('latex', [[
        \usepackage{amsmath,amssymb}
        \usepackage{changepage}
        \usepackage[utf8x]{inputenc}
        \usepackage{textcomp,marvosym}
        \usepackage{cite}
        \usepackage{nameref,hyperref}
        \usepackage[right]{lineno}
        \usepackage{microtype}
        \DisableLigatures[f]{encoding = *, family = * }
        \PassOptionsToPackage{table}{xcolor}
        \usepackage{array}
        \newcolumntype{+}{!{\vrule width 2pt}}
        \newlength\savedwidth
        \newcommand\thickcline[1]{%
          \noalign{\global\savedwidth\arrayrulewidth\global\arrayrulewidth 2pt}%
          \cline{#1}%
          \noalign{\vskip\arrayrulewidth}%
          \noalign{\global\arrayrulewidth\savedwidth}%
        }
        \newcommand\thickhline{\noalign{\global\savedwidth\arrayrulewidth\global\arrayrulewidth 2pt}%
        \hline
        \noalign{\global\arrayrulewidth\savedwidth}}
        \raggedright
        \setlength{\parindent}{0.5cm}
        \textwidth 5.25in
        \textheight 8.75in
        \usepackage[aboveskip=1pt,labelfont=bf,labelsep=period,justification=raggedright,singlelinecheck=off]{caption}
        \renewcommand{\figurename}{Fig}
        \makeatletter
        \renewcommand{\@biblabel}[1]{\quad#1.}
        \makeatother
        \usepackage{lastpage,fancyhdr,graphicx}
        \usepackage{epstopdf}
        \pagestyle{fancy}
        \fancyhf{}
        \rfoot{\thepage/\pageref{LastPage}}
        \renewcommand{\headrulewidth}{0pt}
        \renewcommand{\footrule}{\hrule height 2pt \vspace{2mm}}
        \fancyheadoffset[L]{2.25in}
        \fancyfootoffset[L]{2.25in}
    ]]))

    if not META['header-includes'] then
        META['header-includes'] = {}
    end
    for _, block in ipairs(blocks) do
        table.insert(META['header-includes'], block)
    end

    local authors = pandoc.List()
    assert(#metaget('author') > 0, metapath('author')..' must be a list!')
    for i, author in ipairs(META.author) do
        assert(type(author) == 'table' and #author == 0, metapath('author', i)..' must be a dictionary!')
        for name, a_ in pairs(author) do
            local a = {}
            assert(type(a_) == 'table' and #a_ == 0, metapath('author', i, name)..' must be a dictionary!')
            for k, v in pairs(a_) do
                a[k] = v
            end
            if not a.initials then
                a.initials = get_initials(name)
            end
            a.initials = pandoc.utils.stringify(a.initials)
            a.name = name
            authors:insert(a)
        end
    end
    META.authors = authors

    if not META.geometry then
        META.geometry = {"letterpaper", "top=0.85in", "left=2.75in", "footskip=0.75in"}
    else
        warn('using geometry in YAML header')
    end

    if META['biblio-style'] then
        warn('biblio-style changed for PLOS')
    end
    META['biblio-style'] = 'plos2015'

    return META
end

local function Pandoc(doc)
    local blocks = pandoc.List()
    local maketitle = blocks_maketitle()
    doc.meta.author = maketitle[1]
    blocks:extend(maketitle[2])
    blocks:extend(blocks_text('Abstract'))
    blocks:extend(blocks_text('Author summary', 'simple-abstract'))
    blocks:insert(pandoc.RawBlock('latex', [[
        \linenumbers
    ]]))
    for i, block in ipairs(blocks) do
        table.insert(doc.blocks, i, block)
    end

    blocks = pandoc.List()
    blocks:extend(blocks_text('Acknowledgments'))
    blocks:extend(blocks_text('Funding'))
    blocks:extend(blocks_text('Competing interests', 'conflicts'))
    blocks:extend(blocks_author_contributions())
    blocks:extend(blocks_orcids())
    blocks:insert(pandoc.RawBlock('latex', [[
        \nolinenumbers
    ]]))
    for _, block in ipairs(blocks) do
        table.insert(doc.blocks, block)
    end

    return doc
end

return {{Meta=Meta}, {Pandoc=Pandoc}}
