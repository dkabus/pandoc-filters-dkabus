# PLOS template

Sources:

- <https://journals.plos.org/plosone/s/latex>

## Note

Copy or symlink the files in the <latex> folder to the same folder where you
compile the TeX file to PDF.
