# PNAS template

Sources:

- <https://www.pnas.org/author-center/submitting-your-manuscript#latex>

## Note

Copy or symlink the files in the <latex> folder to the same folder where you
compile the TeX file to PDF.

## Options

A bunch of options can be passed to the documentclass via the metadata:

```yaml
classoption:
- english
- fleqn
- 9pt
- twocolumn
- twoside
- lineno
```
