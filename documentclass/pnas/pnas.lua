META = {}

local latex = function (s) return pandoc.RawBlock('latex', s) end
local str = pandoc.utils.stringify

local function current_script()
  return debug.getinfo(2, "S").source:sub(2)
end

local function warn(...)
    io.stderr:write(table.concat(pandoc.List({'WARNING', current_script(), ...}):map(str), ' ')..'\n')
end
local function warn_missing(...) warn('missing key in metadata:', ...) end

local function is_valid_varname(varname)
    local valid = true
    if varname:sub(1,1):match('%a') then
        for i = 2, #varname do
            if not varname:sub(i,i):match('%w') then
                valid = false
                break
            end
        end
    else
        valid = false
    end
    return valid
end

local function metapath(...)
    local a = ''
    for i, key in ipairs({...}) do
        if type(key) == "number" then
            a = a..'['..key..']'
        elseif not is_valid_varname(str(key)) then
            a = a..'["'..key..'"]'
        else
            if i > 1 then
                a = a..'.'
            end
            a = a..key
        end
    end
    return a
end

local function metaget(...)
    local sel = META
    for _, key in ipairs({...}) do
        sel = sel[key]
    end
    assert(sel, 'MISSING IN METADATA!\t'..metapath(...))
    return sel
end

local function get_last_name(name)
    local last = name:match("{(.*)}")
    if last then
        return last
    else
        return name:match("%S+$")
    end
end

local function get_initials(name)
    local initials = ""
    for i = 1, #name do
        local char = name:sub(i, i)
        if char:match("%u") then
            initials = initials .. char
        end
    end
    return initials
end

local function blocks_institute_info()
    local blocks = pandoc.List()

    local marks = {}
    local institutes = pandoc.List()
    for i, row in ipairs(metaget('institute')) do
        assert(type(row) == 'table', metapath('institute', i)..' must be a table!')
        for key, institute in pairs(row) do
            assert(type(institute) == 'table', metapath('institute', i, key)..' must be a table!')
            marks[key] = string.char(97+((i-1)%26))
            local address = ''
            if institute.name then
                address = address..str(institute.name)
                if institute.addressline then
                    address = address..', '..str(institute.addressline)
                else
                    warn_missing(metapath('institute', i, key, 'addressline'))
                end
                if institute.city then
                    if institute.postcode then
                        address = address..', '..str(institute.postcode)..' '..str(institute.city)
                    else
                        warn_missing(metapath('institute', i, key, 'postcode'))
                        address = address..', '..str(institute.city)
                    end
                else
                    warn_missing(metapath('institute', i, key, 'city'))
                end
                if institute.country then
                    address = address..', '..str(institute.country)
                else
                    warn_missing(metapath('institute', i, key, 'country'))
                end
            else
                warn_missing(metapath('institute', i, key, 'name'))
            end
            institutes:insert({marks[key], address})
        end
    end
    for _, value in ipairs(institutes) do
        blocks:insert(latex('\\affil['..value[1]..']{'..str(value[2])..'}'))
    end

    local correspondence_counter = 0
    local new_authors = pandoc.List()
    for i, author in ipairs(metaget('authors')) do
        assert(type(author) == 'table', metapath('author', i)..' must be a table!')
        assert(type(author.institute) == 'table' and #author.institute > 0, metapath('author', i, 'institute')..' must be a list!')

        local annotations = pandoc.List()
        for _, key in ipairs(author.institute) do
            annotations:insert(marks[str(key)])
        end

        if author.equal then
            warn('Equal contributions are not implemented yet in this documentclass.')
        end

        if author.footnotes then
            warn('Author footnotes are not implemented yet in this documentclass.')
        end

        if author.correspondence then
            correspondence_counter = correspondence_counter + 1
            annotations:insert(correspondence_counter)
            blocks:insert(latex('\\correspondingauthor{\\textsuperscript{'..correspondence_counter..'} To whom correspondence should be addressed. E-mail: '..str(metaget('author', i, author.name, 'email'))..'}'))
        end
        local name = author.name:gsub('{', ''):gsub('}', '')
        blocks:insert(i, latex('\\author['..table.concat(annotations, ',')..']{'..name..'}'))

        new_authors:insert(pandoc.Str(name))
    end

    if #META.authors < 1 then
        error('No authors given!')
    end

    if correspondence_counter == 0 then
        warn_missing(metapath('author', 0, '???', 'correspondence'), '(There is no corresponding author!)')
    end

    return {new_authors, pandoc.Blocks(blocks)}
end

local function blocks_author_contributions()
    local at_least_one = false
    local s = ''
    for _, author in ipairs(META.authors) do
        if author.credit then
            at_least_one = true
            s = s..str(author.initials)..': '
            for i, contribution in ipairs(author.credit) do
                if i > 1 then
                    s = s..', '
                end
                s = s..str(contribution)
            end
            s = s..'. '
        else
            warn_missing(metapath('author', i, author.name, 'credit'))
        end
    end

    if at_least_one then
        return {latex('\\authorcontributions{'..s:gsub('&','\\&')..'}')}
    else
        return {}
    end
end

function Meta(meta)
    META = meta

    if META.documentclass then
        warn('documentclass already defined:', META.documentclass)
    else
        META.documentclass = 'pnas-new'
    end

    for _, key in ipairs({'abstract', 'title', 'date', 'author'}) do
        if not META[key] then
            warn_missing(metapath(key))
        end
    end

    local blocks = pandoc.List()
    blocks:insert(latex([[
        \templatetype{pnasresearcharticle}
        \doi{\url{www.pnas.org/cgi/doi/10.1073/pnas.XXXXXXXXXX}}
        \newcommand{\hide}[1]{}
    ]]))

    if META.keywords then
        local s = ''
        for i, keyword in ipairs(META.keywords) do
            if i > 1 then
                s = s..' $|$ '
            end
            s = s..str(keyword)
        end
        blocks:insert(latex('\\keywords{'..s..'}'))
    else
        warn_missing(metapath('keywords'))
    end

    local authors = pandoc.List()
    assert(#metaget('author') > 0, metapath('author')..' must be a list!')
    for i, author in ipairs(META.author) do
        assert(type(author) == 'table' and #author == 0, metapath('author', i)..' must be a dictionary!')
        for name, a_ in pairs(author) do
            local a = {}
            assert(type(a_) == 'table' and #a_ == 0, metapath('author', i, name)..' must be a dictionary!')
            for k, v in pairs(a_) do
                a[k] = v
            end
            if not a.initials then
                a.initials = get_initials(name)
            end
            a.initials = pandoc.utils.stringify(a.initials)
            a.name = name
            authors:insert(a)
        end
    end
    META.authors = authors

    local instinfo = blocks_institute_info()
    META.author = instinfo[1]
    blocks:extend(instinfo[2])

    if META.authors and #META.authors > 0 then
        blocks:insert(latex('\\leadauthor{'..get_last_name(META.authors[1].name)..'}'))
    end

    if META.conflicts then
        blocks:insert(latex('\\authordeclaration{'..str(META.conflicts)..'}'))
    else
        warn_missing(metapath('conflicts'))
    end

    if META['simple-abstract'] then
        blocks:insert(latex('\\significancestatement{'..str(META['simple-abstract'])..'}'))
    else
        warn_missing(metapath('simple-abstract'))
    end

    blocks:extend(blocks_author_contributions())

    if not META['header-includes'] then
        META['header-includes'] = {}
    end
    for _, block in ipairs(blocks) do
        table.insert(META['header-includes'], block)
    end

    return META
end

local function Pandoc(doc)
    local blocks = pandoc.List()
    blocks:insert(latex([[
        \thispagestyle{firststyle}
        \ifthenelse{\boolean{shortarticle}}{\ifthenelse{\boolean{singlecolumn}}{\abscontentformatted}{\abscontent}}{}
    ]]))
    for i, block in ipairs(blocks) do
        table.insert(doc.blocks, i, block)
    end

    blocks = pandoc.List()
    -- blocks:insert(latex([[
    -- ]]))

    if META.acknowledgments or META.funding then
        local ack = pandoc.List()
        if META.acknowledgments then
            ack:insert(str(META.acknowledgments))
        else
            warn_missing(metapath('acknowledgments'))
        end
        if META.funding then
            ack:insert(str(META.funding))
        else
            warn_missing(metapath('funding'))
        end
        blocks:insert(latex('\\acknow{'..table.concat(ack, '\n\n')..'}\n\n\\showacknow{}'))
    else
        warn_missing(metapath('acknowledgments'))
        warn_missing(metapath('funding'))
    end

    for _, block in ipairs(blocks) do
        table.insert(doc.blocks, block)
    end

    return doc
end

return {{Meta=Meta}, {Pandoc=Pandoc}}
