#!/bin/sed -f
s/\\usepackage\(\[[^]]*\]\)\?{\(lmodern\|geometry\|graphicx\|selnolig\)}\(\s*%.*\)\?//g;
s/\\usepackage\(\[\([^]]*\)\]\)\?{\(natbib\)}/\\PassOptionsToPackage{\2}{\3}/g;
0,/\\date\s*{/s/\\date\s*{/\\dates{/g;
0,/\\author\s*{/s/\\author\s*{/\\hide{/g;
s/\\bibliographystyle\s*{/\\hide{/g;
