META = {}
INSTITUTE_META_ORDER = {'name', 'addressline', 'postcode', 'city', 'country'}

local str = pandoc.utils.stringify

local function current_script()
  return debug.getinfo(2, "S").source:sub(2)
end

local function warn(...)
    io.stderr:write(table.concat(pandoc.List({'WARNING', current_script(), ...}):map(str), ' ')..'\n')
end
local function warn_missing(...) warn('missing key in metadata:', ...) end

local function is_valid_varname(varname)
    local valid = true
    if varname:sub(1,1):match('%a') then
        for i = 2, #varname do
            if not varname:sub(i,i):match('%w') then
                valid = false
                break
            end
        end
    else
        valid = false
    end
    return valid
end

local function metapath(...)
    local a = ''
    for i, key in ipairs({...}) do
        if type(key) == "number" then
            a = a..'['..key..']'
        elseif not is_valid_varname(str(key)) then
            a = a..'["'..key..'"]'
        else
            if i > 1 then
                a = a..'.'
            end
            a = a..key
        end
    end
    return a
end

local function metaget(...)
    local sel = META
    for _, key in ipairs({...}) do
        sel = sel[key]
    end
    assert(sel, 'MISSING IN METADATA!\t'..metapath(...))
    return sel
end

local function latex_header()
    local l = pandoc.List()

    l:insert('\\title[mode=title]{'..str(metaget('title'))..'}')
    l:insert('\\tnotemark[1]')
    l:insert('\\tnotetext[1]{'..str(metaget('date'))..'}')

    local shorttitle = ''
    if META.shorttitle then
        shorttitle = META.shorttitle
    else
        warn_missing(metapath('shorttitle'))
        shorttitle = META.title
    end
    l:insert('\\shorttitle{'..str(shorttitle)..'}')

    local marks = {}
    if META.institute and #META.institute > 0 then
        for i, row in ipairs(META.institute) do
            assert(type(row) == 'table', metapath('institute', i)..' must be a table!')
            for key, institute in pairs(row) do
                assert(type(institute) == 'table', metapath('institute', i, key)..' must be a table!')
                marks[str(key)] = i
                l:insert('\\affiliation['..i..']{%')
                l:insert('  postcodesep={},')
                for _, label in ipairs(INSTITUTE_META_ORDER) do
                    local value = institute[label]
                    if value then
                        if label == 'name' then
                            label = 'organization'
                        end
                        l:insert('  '..str(label)..'={'..str(value)..'},')
                    else
                        warn_missing(metapath('institute', i, key, label))
                    end
                end
                l:insert('}')
            end
        end
    else
        warn_missing(metapath('institute'), '(must be a list)')
    end

    local footnote_count = 0
    local footnotes = {}
    local have_corresponding = false
    for i, author in ipairs(metaget('authors')) do
        assert(type(author) == 'table', metapath('author', i)..' must be a table!')
        assert(type(author.institute) == 'table' and #author.institute > 0, metapath('author', i, 'institute')..' must be a list!')

        local inst = pandoc.List()
        for _, key in ipairs(author.institute) do
            local mark = marks[str(key)]
            assert(mark, 'Can not find an institute with key: '..str(key))
            inst:insert(mark)
        end

        l:insert('\\author['..table.concat(inst, ',')..']{'..str(author.name)..'}[')
        for k, v in pairs(author) do
            if k == 'orcid' then
                l:insert('  '..str(k)..'='..str(v)..',')
            end
        end
        l:insert(']')

        if author.credit then
            l:insert('\\credit{'..table.concat(author.credit:map(str), ', '):gsub('&', '\\&')..'}')
        else
            warn_missing(metapath('author', i, author.name, 'credit'))
        end

        if author.correspondence then
            l:insert('\\ead{'..str(metaget('author', i, author.name, 'email'))..'}')
            l:insert('\\cormark[1]')
            have_corresponding = true
        end

        if author.equal then
            if not author.footnotes then
                warn_missing(metapath('author', i, author.name, 'footnotes'))
                author.footnotes = {}
            end
            author.footnotes['equal'..str(author.equal)] = 'Contributed equally'
        end

        if author.footnotes then
            local fnmarks = pandoc.List()
            for key, description in pairs(author.footnotes) do
                local k = str(key)
                if not footnotes[k] then
                    footnote_count = footnote_count + 1
                    footnotes[k] = {num=footnote_count, desc=str(description)}
                end
                fnmarks:insert(footnotes[k].num)
            end
            l:insert('\\fnmark['..table.concat(fnmarks,',')..']')
        else
            warn_missing(metapath('author', i, author.name, 'footnotes'))
        end
    end

    if #META.authors < 1 then
        error('No authors given!')
    elseif #META.authors == 1 then
        l:insert('\\shortauthors{'..str(META.authors[1].name)..'}')
    else
        l:insert('\\shortauthors{'..str(META.authors[1].name)..' et al.}')
    end

    if not have_corresponding then
        warn_missing(metapath('author', 0, '???', 'correspondence'), '(There is no corresponding author!)')
    end

    if META.keywords then
        l:insert('\\begin{keywords}')
        for j, keyword in ipairs(META.keywords) do
            if j == 1 then
                l:insert(str(keyword))
            else
                l:insert('\\sep '..str(keyword))
            end
        end
        l:insert('\\end{keywords}')
    else
        warn_missing(metapath('keywords'))
    end

    if META['graphical-abstract'] then
        l:insert('\\begin{graphicalabstract}')
        l:insert('\\includegraphics{'..str(META['graphical-abstract'])..'}')
        l:insert('\\end{graphicalabstract}')
    else
        warn_missing(metapath('graphical-abstract'))
    end

    if META.highlights then
        l:insert('\\begin{highlights}')
        for _, highlight in ipairs(META.highlights) do
            l:insert('\\item '..str(highlight))
        end
        l:insert('\\end{highlights}')
    else
        warn_missing(metapath('highlights'))
    end

    local footnotes_sorted = {}
    for _, fn in pairs(footnotes) do
        footnotes_sorted[fn.num] = fn.desc
    end
    for num, desc in ipairs(footnotes_sorted) do
        l:insert('\\fntext['..num..']{'..desc..'}')
    end

    l:insert('\\cortext[1]{Corresponding author}')
    l:insert('\\maketitle')
    return table.concat(l, '\n')
end

local function blocks_section(heading, blocks)
    local header = pandoc.Header(1, heading)
    header.classes = {'unlisted', 'unnumbered'}
    return pandoc.Blocks{header, table.unpack(blocks)}
end

function Meta(meta)
    META = meta

    for _, key in ipairs({'abstract', 'title', 'date', 'author'}) do
        if not META[key] then
            warn_missing(metapath(key))
        end
    end

    local blocks = pandoc.List()
    blocks:insert(pandoc.RawBlock('latex', [[
        \newcommand{\hide}[1]{}
    ]]))

    if not META['header-includes'] then
        META['header-includes'] = {}
    end
    for _, block in ipairs(blocks) do
        table.insert(META['header-includes'], block)
    end

    if META.documentclass then
        warn('documentclass already defined:', META.documentclass)
    else
        META.documentclass = 'cas-dc'
    end

    local authors = pandoc.List()
    assert(#metaget('author') > 0, metapath('author')..' must be a list!')
    for i, author in ipairs(META.author) do
        assert(type(author) == 'table' and #author == 0, metapath('author', i)..' must be a dictionary!')
        for name, a_ in pairs(author) do
            local a = {}
            assert(type(a_) == 'table' and #a_ == 0, metapath('author', i, name)..' must be a dictionary!')
            for k, v in pairs(a_) do
                a[k] = v
            end
            a.name = name
            authors:insert(a)
        end
    end
    META.authors = authors

    return META
end

local function Pandoc(doc)
    local blocks = pandoc.List()
    blocks:insert(pandoc.RawBlock('latex', latex_header()))
    for i, block in ipairs(blocks) do
        table.insert(doc.blocks, i, block)
    end

    blocks = pandoc.List()

    if not META.title then
        warn_missing(metapath('title'))
    end

    if META.acknowledgments then
        blocks:extend(blocks_section('Acknowledgments', META.acknowledgments))
    else
        warn_missing(metapath('acknowledgments'))
    end

    if META.funding then
        blocks:extend(blocks_section('Funding', META.funding))
    else
        warn_missing(metapath('funding'))
    end

    if META.conflicts then
        blocks:extend(blocks_section('Competing interests', META.conflicts))
    else
        warn_missing(metapath('conflicts'))
    end

    blocks:insert(pandoc.RawBlock('latex', [[
        \printcredits
    ]]))

    for _, block in ipairs(blocks) do
        table.insert(doc.blocks, block)
    end

    return doc
end

return {{Meta=Meta}, {Pandoc=Pandoc}}
