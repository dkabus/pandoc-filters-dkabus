# Elsevier template

Sources:

- <https://www.elsevier.com/authors/policies-and-guidelines/latex-instructions>
- <https://www.ctan.org/pkg/els-cas-templates/>

## Options

By default, we use the two-column layout.
To use a one-column layout, in the metadata of your document, set:

```yaml
documentclass: cas-sc
```
