#!/bin/sed -f
s/\\usepackage\(\[[^]]*\]\)\?{\(lmodern\|geometry\|graphicx\)}//g;
0,/\\begin{document}/{s/\\\(title\|author\|date\)\s*{/\\hide{/g;};
0,/\\maketitle/{s/\\maketitle//};
