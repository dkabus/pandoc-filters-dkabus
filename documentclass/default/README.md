# Default template

## Options

By default, we use the one-column layout.
To use a two-column layout, in the metadata of your document, set:

```yaml
header-includes: |
    ```{=latex}
    \usepackage{multicol}
    \twocolumn
    ```
```
