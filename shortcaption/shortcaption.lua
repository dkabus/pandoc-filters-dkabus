function remove_trailing_full_stop(inlines)
    if #inlines > 0 then
        local last = inlines[#inlines]
        if last.t == "Str" then
            last.text = last.text:gsub("%.$", "")
            if last.text == "" then
                table.remove(inlines, #inlines)
            end
        end
    end
    return inlines
end

local function set_short_caption(el)
    -- if unset, set the short caption of figures and tables to the first bold element
    if el.caption and el.caption.long and not el.caption.short then
        local short = nil
        el.caption.long:walk {
            Strong = function (strong)
                if short == nil then
                    short = remove_trailing_full_stop(strong.content)
                end
            end,
        }
        el.caption.short = short
        return el
    end
end

return {{ Figure = set_short_caption, Table = set_short_caption }}
