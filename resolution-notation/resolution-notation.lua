--[[
        Convert resolution notation 100x100 to $100 \times 100$
--]]

local function lStr(s)
    return pandoc.RawInline('latex', s)
end

local function lMath(s)
    return pandoc.Math(s)
end

local function replace_inline_math(elem)
    if elem.t == 'Str' then
        local replaced = string.gsub(elem.text, "(%d+)x(%d+)", "$%1 \\times %2$")
        if replaced ~= elem.text then
            return pandoc.walk_inline(lStr(replaced), {Str = lStr, Math = lMath})
        end
    end
end

return {
    {
        Str = replace_inline_math
    }
}
